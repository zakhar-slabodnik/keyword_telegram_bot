require "keyword_bot/version"
require "telegram/bot"

TOKEN = '287869932:AAGy7TTnASJhA9jYHd_LHnw_aQeEVjF0hMI'

module KeywordBot
  KEYWORDS = [["language", "exmp1", "exmp2", "exmp3"], ["test", "exmp4", "exmp5", "exmp6"], ["ping", "exmp7", "exmp8", "exmp9"]]
  
  def self.search(keyword)
    relate = KEYWORDS.find { |related| related.include?(keyword) }
    if !relate.nil?
      relate.to_a.join(', ')
    else
      "Type only 'language, 'test' or 'ping'"
    end
  end

  def self.start
    Telegram::Bot::Client.run(TOKEN) do |bot|
      bot.listen do |message|
        if message.text.include? ?/
          case message.text
          when '/start'
            bot.api.sendMessage(
              chat_id: message.chat.id,
              text: "Hello, #{message.from.first_name}! You can type any existing keyword and get 10 relations")
          when '/stop'
            bot.api.sendMessage(
              chat_id: message.chat.id,
              text: "Bye, #{message.from.first_name}!")
          else
            bot.api.send_message(
              chat_id: message.chat.id,
              text: "Use only /start, /stop comands and existing words")
          end
        else
          case message
          when Telegram::Bot::Types::Message
            bot.api.send_message(
              chat_id: message.chat.id,
              text: KeywordBot.search(message.text))
          end
        end
      end
    end
  end
end
