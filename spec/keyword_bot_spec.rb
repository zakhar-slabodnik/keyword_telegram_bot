require "spec_helper"

RSpec.describe KeywordBot do
  it "should search by keyword using yandex" do
    terms = KeywordBot.search("language")
    expect(terms.size).to eq(10)
  end
end
